@extends('layout.master')
@section('judul_1')
    Buat Account Baru
@endsection
@section('judul_2')
    Sign Up Form
@endsection

@section('content')
<form action="welcome" method="post">
    @csrf
    <div>
        <label>First Name :</label>
        <div>
            <input type="text" name="first_n" required>
        </div>
    </div>
    <br>
    <div>
        <label>Last Name :</label>
        <div>
            <input type="text" name="last_n" required>
        </div>
    </div>
    <br>
    <div>
        <label>Gender</label>
        <div>
            <input type="radio" name="jk" value="l">Male <br>
            <input type="radio" name="jk" value="p">Female
        </div>
    </div>
    <br>
    <div>
        <label>Nationality</label>
        <div>
            <select name="nat" required>
                <option value="ina">Indonesia</option>
                <option value="us">Amerika</option>
                <option value="en">Inggris</option>
            </select>
        </div>
    </div>
    <br>
    <div>
        <label>Language Spoken</label>
        <div>
            <input type="checkbox" id="id" name="lang" value="id">
            <label for="id">Bahasa Indonesia</label> <br>
            <input type="checkbox" id="en" name="lang" value="en">
            <label for="en">English</label> <br>
            <input type="checkbox" id="ot" name="lang" value="oth">
            <label for="ot">Other</label> <br>
        </div>
    </div>
    <br>
    <div>
        <label>Bio</label>
        <div>
            <textarea name="bio" cols="26" rows="8" required></textarea>
        </div>
    </div>
    <div>
        <input type="submit" name="signup" value="Sign Up">
    </div>
</form>
@endsection
