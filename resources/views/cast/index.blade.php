@extends('layout.master')
@section('judul_1')
Halaman List Table
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary">Add Cast</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Cast</th>
        <th scope="col">Description</th>
        <th scope="col">Handle</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$key->description}}</td>
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-info btn-sm">Edit</a>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            
        @endforelse
    </tbody>
  </table>
@endsection