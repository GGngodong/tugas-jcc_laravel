@extends('layout.master')
@section('judul_1')
Halaman Data Table
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="exampleInputEmail1">Cast </label>
      <input type="text" class="form-control" name="cast">
    </div>
    @error('cast')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleInputPassword1">Description</label>
      <textarea name="description" class="form-control" ></textarea>
    </div>
    @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection